# seen

*A simple blog generator*

### Facts

- Doesn't depend on GNU extensions

- POSIX compliant

- Works on non-GNU distros (such as Alpine)

- Works on BSD systems (such as MacOS and OpenBSD)

- Depends on `markdown`/`hoedown`

- Less than 100 SLOC

### How to use

First, install `markdown`/`hoedown` because this script uses it heavily.

Put your markdown files on `articles/<name of file>.md` (create the directory if it doesn't exist).

You also need to create a config file. Put it in `articles/<name of file>.cfg`. It should use the same name your markdown file uses:

	name="Hello World"
	desc="Welcome to my blog!"
	date="9th December 2021"

You can create RSS feeds too, just modify the `rss.cfg` file to your liking:

	rssEnabled="y" # Enable RSS
	rssBlogTitle="foobar's blog"
	rssBlogDescription="foobar loves cooking!!!!!!!"

And run the following:

	$ chmod +x seen.sh
	$ ./seen.sh

Your blog is in the `www/` directory. You can make your webserver use it.
You can also use CSS files, just put them into the `templates/` directory.

Feel free to customize other html files present in that directory.

Seen supports other options too, for more info, run the following:

	$ ./seen.sh --help

