# Hello World

If you're reading this, that means `seen` has been successfully
configured and is working!

Feel free to remove this article from your website.

For more info, please read the [README.md](http://vitali64.duckdns.org/?p=utils/seen.git;a=blob;f=README.md;hb=HEAD) file at the root of the repo.

Thanks for using `seen`.

### License

`seen` is licensed under the GNU GPLv3 License as published by the FSF, 
for more information, please read the LICENSE file at the root of the repo.

~ Vitali64
